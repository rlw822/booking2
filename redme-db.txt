create database booking2 (std phpmyadmin)


use booking2;

DROP TABLE IF EXISTS bookings;
CREATE TABLE bookings (
    id int auto_increment
    , booking_date DATE
    , am1 VARCHAR(15), pm1 VARCHAR(15)
    , am2 VARCHAR(15), pm2 VARCHAR(15)
    , am3 VARCHAR(15), pm3 VARCHAR(15)
    , primary key (id)
);